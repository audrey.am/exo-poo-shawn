import AnimalSexe from "../class/AnimalSexe";

class Animal {
  // le nom de la classe a.k.a le "type" des moutons
  readonly nom: string; // le nom du mouton. Il peut être lu par les autres
  // objet d'autres classes mais
  protected sexe: AnimalSexe; // pas modifié (on ne peut pas renommer un mouton)
  protected age: number; 
  protected fecondite: boolean;
  protected genre :string;// l'age du mouton, c'est privé, les autres classe
  // ne peuvent pas savoir ce qu'il y a dedans

  constructor(nom: string, age: number, sexe: AnimalSexe = AnimalSexe.female) {
    // permet de créer un nouveau mouton avec 'new'
    this.nom = nom; // par exemple `new Mouton ("kevin", 16) créé
    this.age = age >= 0 ? age : 0;
    this.sexe = sexe;
    this.fecondite = true;
   
    if (this.sexe === AnimalSexe.female) {
      this.genre = "la femme";
    } else {
      this.genre = "le male";
    }
    // un Mouton qui s'appelle Kévin et a 16 ans
  }

  vieillit() {
    // Quand on appelle cette méthode sur un mouton son age
    this.age++; // augmente de 1 c'est le seul moyen de modifier l'age
  } // d'un mouton ce qui évite de le faire rajeunir par exemple
  
  parle() {
  
    let message: string =
      // comme elle utilise le nom et l'age le résultat sera
      `je suis ${this.nom} ${this.genre} et j'ai ${this.age} ans`;
    console.log(message); // unique pour chaque mouton (à peu près)
    return message;
  }

  copuler(partenaire: Animal, typeEnfant) {
    if (this.sexe !== partenaire.sexe && this.fecondite && partenaire.fecondite) {
      if (this.age > 4 && partenaire.age > 4) {
        let sexeAleatoire: AnimalSexe = AnimalSexe.female;
        if (Math.random() > 0.5) {

        this.fecondite = (this.sexe === AnimalSexe.female)?false : true;
        partenaire.fecondite = (partenaire.sexe === AnimalSexe.male)? false : true;
          sexeAleatoire = AnimalSexe.male;
          let bebe = new typeEnfant(
            this.nom.concat(partenaire.nom),
            0,
            sexeAleatoire
          );
           return bebe;
        

        }
      } else {
        console.log("Un des animaux est trop jeune");
      }
    } else {
      console.log("L'animal va voir ailleurs");
    }
  }
} //Mouton


export default Animal;
