import AnimalSexe from "../class/AnimalSexe";
import Animal from "./Animal";
let nid: Oeuf[] = [];
export class Poule extends Animal {
  constructor(nom: string, age: number, sexe: AnimalSexe = AnimalSexe.female) {
    super(nom, age, sexe);
    this.genre = this.sexe === AnimalSexe.female ? "la Poule" : "Le coq";
  }
  pondre() {
    return nid.push(new Oeuf("billy", 0));
  }

  copuler(partenaire: Poule): Poule {
    return super.copuler(partenaire, Oeuf);
  }
} //Poule

export class Oeuf extends Animal {
  eclore() {
    
    return new Poule(this.nom, 0);
    
  }
  
}
