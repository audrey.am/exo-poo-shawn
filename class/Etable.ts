import Animal from "./Animal";
import { Oeuf }  from "./Poule";
import { Poule }  from "./Poule";


export class Etable{
    protected animaux: Animal[] = []
    readonly nbPlaceMax: number

    constructor(nbPlace: number) {
        this.nbPlaceMax = nbPlace
    }

    ajouter(Animal:Animal){
        this.animaux.push(Animal)
    }
    retirer(Animal:Animal):Animal{
        let i = this.animaux.indexOf(Animal)
        delete this.animaux[i]
        return Animal
    }
    vider():Animal[]{
        let tableau : Animal[] = [];
        this.animaux.forEach(e=> tableau.push(e));
        this.animaux = [];
        return tableau;
        
    }
    vieillir(){
        this.animaux.forEach(a => a.vieillit())
    }
    afficher(){
        this.animaux.forEach(a => a.parle())
    }
}

export class Poulailler extends Etable{
    protected animaux: Poule[] = [];
    protected nid: Oeuf[] = [];
    protected panier: Oeuf[]=[];
    pondre() {
        
        this.animaux.forEach(poulette => poulette.pondre())
        this.nid.forEach(o=> {this.nid.push(o)})
      }
    // allerChercherLesOeufs(n): Oeuf[] {
        
    //     this.nid.forEach(n => {this.panier.push(n)
    //     });
    //   }
}
