// la classe des moutons
import AnimalSexe from "../class/AnimalSexe";
import Animal from "./Animal";

class Vache extends Animal{


  constructor(nom: string, age: number, sexe: AnimalSexe = AnimalSexe.female) {
      super(nom, age, sexe)
      if (this.sexe === AnimalSexe.female) {
        this.genre = "la vache";
      } else {
        this.genre = "le taureau";
      }
    
  }


  copuler(partenaire: Vache): Vache {
      return super.copuler(partenaire, Vache)
  }
} //Vache


export default Vache
