// la classe des moutons
import AnimalSexe from "../class/AnimalSexe";
import Animal from "./Animal";

class Mouton extends Animal{


  constructor(nom: string, age: number, sexe: AnimalSexe = AnimalSexe.female) {
      super(nom, age, sexe)
      if (this.sexe === AnimalSexe.female) {
        this.genre = "la brebis";
      } else {
        this.genre = "le mouton";
      }
    
  }


  copuler(partenaire: Mouton): Mouton {
      return super.copuler(partenaire, Mouton)
  }
} //Mouton


export default Mouton
